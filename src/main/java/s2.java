import javax.swing.*;
import java.awt.event.*;

public class s2 extends JFrame {

    JTextField textField;
    JTextArea textArea;
    JButton button;


    s2() {
        setTitle("Subiect 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
        setLocation(500,400);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        button = new JButton("Muta textul");
        button.setBounds(90, 150, 100, height);

        textField = new JTextField(4);
        textField.setBounds(50, 50, 200, height);

        textArea = new JTextArea(15, 15);
        textArea.setBounds(50, 90, 200, height);
        textArea.setEditable(false);


        button.addActionListener(new Action() {
            public void actionPerformed(ActionEvent ae) {
                textArea.setText(textField.getText());
            }
        });


        add(button);
        add(textField);
        add(textArea);


    }

    public static void main(String[] args) {
        new s2();
    }

    class Action implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            //This is the code that should  perform the task
            String name = textField.getText();
            textArea.append(name);
        }
    }
}

