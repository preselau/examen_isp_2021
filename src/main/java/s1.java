import java.util.ArrayList;
import java.util.List;

public class s1 {
    class A {

    }

    class B extends A { // mostenire
        private String param;

        // mai jos avem legatura de compozitie
        C[] list = new C[2];

        B() {
            list[0] = new C();
            list[1] = new C();
        }//incheiere legatura de compozitie


        // mai jos avem agregarea
        List<E> eList = new ArrayList<E>();

        public void addE(E e) {
            eList.add(e);
        }//incheiere agregare

        Z zObj; // dependenta
    }

    class C {

    }

    class D {
        B bObject; // asociere unidirectionala

        public void f() {

        }

    }

    class E {

    }

    class Z {
        public void g() {

        }
    }
}